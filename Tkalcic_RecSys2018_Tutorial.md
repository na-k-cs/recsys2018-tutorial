---
title       	: Emotions and Personality in Recommender Systems
subtitle    	: 
author      	: Marko Tkal\v{c}i\v{c}, Free University of Bozen-Bolzano
institute		: ACM RecSys 2018 Tutorial
logo        	: lib/unibz-logo.png
mode        	: selfcontained # {standalone, draft}
theme			: metropolis
footer 			: Marko Tkal\v{c}i\v{c}, RecSys2018-AffectRecSys-Tutorial
shownotes		: true
section-titles	: false
fontsize		: 8pt
---


# Introduction


## Who am I?

Marko Tkalčič

[columns]

[column=0.7]

- 2016 - now : assistant professor at Free University of Bozen-Bolzano
- 2013 - 2015: postdoc at Johannes Kepler University, Linz
- 2011 - 2012: postdoc at University of Ljubljana 
- 2008 - 2010: PhD student at University of Ljubljana

[column=0.3]

![](./figures/markot.jpg)

[/columns]

My research explores ways in which **psychologically-motivated user characteristics**, such as emotions and personality, can be used to improve **recommender systems** (personalized systems in general). It employs methods such as **user studies** and **machine learning**. 




## Book, 2016

[columns]

[column=0.7]

- Tkalčič, M., Carolis, B. De, Gemmis, M. de, Odić, A., & Košir, A. (Eds.). (2016). Emotions and Personality in Personalized Services. Springer International Publishing. https://doi.org/10.1007/978-3-319-31413-6
- Authors from
	- Stanford, Cambridge, Imperial College, UCL ...  
- Topics:
	- psychological models
	- acquisition of emotions/personality
	- personalization techniques
- http://www.springer.com/gp/book/9783319314112

[column=0.3]

![](./figures/book_snapshot.png)

[/columns]



## Book, 2018

[columns]

[column=0.7]

- Felfernig, A., Boratto, L., Stettinger, M., & Tkalčič, M. (2018). Group Recommender Systems. Springer International Publishing. https://doi.org/10.1007/978-3-319-75067-5
- Topics:
	- group recsys algorithms
	- group recsys evaluation
	- explanations
	- applications
	- biases, personality, emotions
- https://www.springer.com/gp/book/9783319750668

[column=0.3]

![](./figures/grprecsysbook.jpg)

[/columns]



## DMRS Workshop

- International Workshop on Decision Making and Recommender Systems 2018
- 29-30 November 2018, Bozen-Bolzano, Italy
- Program:
	- Is Algorithmic Justice an Oxymoron? **Allison Stanger**, Middlebury College, USA 
	- Platform Health Metrics, **Paul Resnick**, University of Michigan, USA 
	- Diversity: a concept with a mission, and what that means for diverse recommender design, **Natali Helberger**, University of Amsterdam, The Netherlands 
	-  Predicting Human Decision-Making: From Prediction to Recommendation, **Ariel Rosenfeld**, Weizmann Institute of Science, Israel 
	- Diversity maximization in social networks, **Aristides Gionis**, Aalto University, Finland 
	- Good City Life, **Daniele Quercia**, Bell Labs, Cambridge, UK 
- Organizers:
	- Francesco Ricci
	- Markus Zanker
	- Marko Tkalcic
	- Mehdi Elahi
- for registration:
	- submit abstract to https://easychair.org/conferences/?conf=dmrs2018
	- extended deadline: 8. October 2018

[http://dmrsworkshop.inf.unibz.it/index.php](http://dmrsworkshop.inf.unibz.it/index.php)

 


## Goal of this Talk/Learning Outcomes

- bare-minimum psychology understanding
- how to emotions/personality in RecSys algorithms
- how to acquire emotions (hands-on)


. . .

- all material related to this tutorial can be found:

[https://gitlab.com/markotka/recsys2018-tutorial](https://gitlab.com/markotka/recsys2018-tutorial)





## Goal of this Talk/Learning Outcomes

[columns]

[column=0.5]

\vspace{0.5cm}

Recommender Systems

\vspace{1.5cm}

Tutorial

- some proven solutions
- ideas for research

\vspace{1.5cm}

Psychology

[column=0.5]

![](./figures/football-1.pdf){width=100%} 

[/columns]








































# Motivation


## Personality, Mood and Emotions

![](./figures/slide2.png)

## Personality, Mood and Emotions

![](./figures/slide3.png)

## Personality, Mood and Emotions

![](./figures/slide4.png)

## Personality, Mood and Emotions

![](./figures/slide5.png)

## Personality, Mood and Emotions

![](./figures/slide6.png)

## Personality, Mood and Emotions

![](./figures/slide7.png){width=100%}




## Personality

- What is personality?
	- accounts for individual differences ( = explains the variance in users) in our enduring emotional, interpersonal, experiential, attitudinal, and motivational styles


. . .


- The five factor model (FFM) – Big5:
	- Extraversion
	- Agreeableness
	- Conscientousness
	- Neuroticism (inverse = Emotional Stability)
	- Openness (to new experiences)


\begin{reference}{}
McCrae, R. R., and John, O. P. (1992). An Introduction to the Five-Factor Model and its Applications. Journal of Personality, 60(2), p175-215.
\end{reference}





## Individual Factors

- **Openness to Experience (O)**, 
	- high: imaginative, creative people (individualistic, non conforming and are very aware of their feelings)
	- low: down-to-earth, conventional people. (simple and straightforward thinking over complex, ambiguous and subtle)
	- sub-factors are imagination, artistic interest, emotionality, adventurousness, intellect and liberalism.
- **Conscientiousness**
	- high : prudent, organized 
	- low: impulsive, disorganized.
	- sub-factors are self-efficacy, orderliness, dutifulness, achievement-striving, self-discipline and cautiousness. 
- **Extraversion**
	- high: degree of engagement with the external world, react with enthusiasm and often have positive emotions
	- low: lack of engagement with the external world, quiet, low-key and disengaged in social interactions.
	- sub-factors of E are friendliness, gregariousness, assertiveness, activity level, excitement-seeking and cheerfulness.

## Individual Factors

- **Agreeableness**
	- high : need for cooperation and social harmony
	- low : opposite
	- subfactors: trust, morality, altruism, cooperation, modesty and sympathy.
- **Neuroticism**
	- high: emotionally reactive. They tend to respond emotionally to relatively neutral stimuli. They are often in a bad mood, which strongly affects their thinking and decision making
	- low: calm, emotionally stable and free from persistent bad mood.
	- sub-factors are anxiety, anger, depression, self-consciousness, immoderation and vulnerability. 










## Thomas-Kilmann Conflict Mode

- was developed to measure the conflict resolution styles in groups

![](./figures/ThomasKillman.pdf)

\begin{reference}{}
Thomas, K. L., and Kilman, R. H. (n.d.). Thomas-Kilman Conflict Mode Instrument.
\end{reference}







## Emotion vs. Mood vs. Sentiment

Let's clear some terminology

- **Affect** : umbrella term for describing the topics of emotion, feelings, and moods
- **Emotion**: 
	- brief in duration 
	- consist of a coordinated set of responses (verbal, physiological, behavioral, and neural mechanisms)
	- triggered
- **Mood**: 
	- last longer
	- less intense than emotions
	- no trigger
- **Sentiment**: 
	- towards an object
	- positive/negative










## Models of Emotions

- Emotions are complex human experiences
- Evolutionary based
- Several definitions, we take with simple models, easy to incorporate in computers:
	- Basic emotions
	- Dimensional model




## Basic Emotions

- Discrete classes model
- Different sets
- Darwin: Expression of emotions in man and animal

[columns]

[column=0.6]

- Ekman definition (6 + neutral):
	- Happiness
	- Anger
 	- Fear
	- Sadness
	- Disgust
	- Surprise

[column=0.4]

![](./figures/ekman_emotions.png)

[/columns]





## Dimensional model of Emotions

[columns]

[column=0.5]

Three continuous dimensions

>- Valence/Pleasure (positive-negative)
>- Arousal (high-low )
>- Dominance (high-low )

Each emotion is a point in the VAD space

[column=0.5]

![](./figures/sam.png)

Self-Assessment Manikin (SAM)

[/columns]

![](./figures/va-emotions.gif){width=25%}

\begin{reference}{}
Bradley, M. M., and Lang, P. J. (1994). Measuring emotion: the self-assessment manikin and the semantic differential. Journal of Behavior Therapy and Experimental Psychiatry, 25(1), 49–59.
\end{reference}




## Personality and preferences

Personality traits (extraverted/introverted, open/conservative etc.) are linked to music genre preferences (Rentfrow et al, 2003)

![](./figures/rentfrow-doremi.png)

\begin{reference}{}
Rentfrow, P. J., and Gosling, S. D. (2003). The do re mi’s of everyday life: The structure and personality correlates of music preferences. Journal of Personality and Social Psychology, 84(6), 1236–1256. \\
Tkalčič, M., Ferwerda, B., Hauger, D., and Schedl, M. (2015). Personality Correlates for Digital Concert Program Notes. In UMAP 2015, Lecture Notes On Computer Science 9146 (Vol. 9146, pp. 364–369).
\end{reference}




## Choice is driven by emotions

Why we choose to consume some kind of content?

. . .

One of the main reasons why people consume music (Lonsdale, 2011) and films (Oliver, 2008) is **emotion regulation.**

![](./figures/uses-gratification-music.png)

\begin{reference}{}
Lonsdale, A. J., and North, A. C. (2011). Why do we listen to music? A uses and gratifications analysis. British Journal of Psychology (London, England : 1953), 102(1), 108–34. https://doi.org/10.1348/000712610X506831

Oliver, M. B. (2008). Tender affective states as predictors of entertainment preference. Journal of Communication, 58(1), 40–61. https://doi.org/10.1111/j.1460-2466.2007.00373.x
\end{reference}



## Decision making model - Lerner

![](./figures/decision_making_emotions.png){width=90%}

**emotions and personality** influence our decisions!

\begin{reference}{}
Lerner, J. S., Li, Y., Valdesolo, P., and Kassam, K. S. (2015). Emotion and Decision Making. Annual Review of Psychology, 66(1), 799–823. 
\end{reference}



## Intention of content creators

>- *“A film is - or should be - more like music than like fiction. It should be a progression of moods and feelings. The theme, what's behind the emotion, the meaning, all that comes later.”* -- Stanley Kubrick
>- *“If my films make one more person miserable, I'll feel I have done my job.”* -- Woody Allen
>- *“Through careful manipulation and good storytelling, you can get everybody to clap at the same time, to laugh at the same time, and to be afraid at the same time.”* -- Steven Spielberg




## Quotes

Claudia Perlich, keynote @Recsys 2016
- Q: *In the world of deep learning, XGBoost etc. why logistic regression?*
- A: *I studied **neural networks** in 1995 ... downgraded to **decision trees** in 2004 and won 3 KDD cups with **linear models**. Personally I find it easier to build up a model with **interesting feature construction**...I can look under the hood and see what these things are doing.*

\begin{reference}{}
https://www.youtube.com/watch?v=1WmqqfXNFZ4\&feature=youtu.be\&t=3003
\end{reference}

. . .

*To get computers to think like humans, we need a new A.I. paradigm, one that places **top down** and **bottom up** knowledge on equal footing. Bottom-up knowledge is the kind of **raw information** we get directly from our senses, like patterns of light falling on our retina. Top-down knowledge comprises **cognitive models** of the world and how it works.*

\begin{reference}{}
Gary Marcus, Artificial Intelligence Is Stuck. Here’s How to Move It Forward. New York Times, July 29, 2017 
\end{reference}











































# Personality-based recommendations

## Usage of personality in RecSys

- assume personality can be measured

. . .

- where can personality be used?
	- content recommendation (ruled-based classifiers)
	- new user problem (user similarity, MF)
	- diversity (ruled-based classifiers)
	- group recommendations 
	 


## Personality for mood regulation

![](./figures/ferwerda-mood-regulation.png)

- high on openness, extraversion, and agreeableness more inclined to listen to happy music when they are feeling sad.
- high on neuroticism listen to more sad songs when feeling disgusted (neurotic people choose to increase their level of worry)

\begin{reference}{}
Ferwerda, B., Schedl, M., and Tkalcic, M. (2015). Personality and Emotional States : Understanding Users ’ Music Listening Needs. In A. Cristea, J. Masthoff, A. Said, and N. Tintarev (Eds.), UMAP 2015 Extended Proceedings.
\end{reference}



## Personality and music browsing styles

- personality is correlated with music browsing styles

[columns]

[column=0.6]

![](./figures/tuneafind.png)

[column=0.4]

![](./figures/pers-style.png)


[/columns]

\begin{reference}{}
Ferwerda, B., Yang, E., Schedl, M., and Tkalčič, M. (2015). Personality Traits Predict Music Taxonomy Preferences. In Proceedings of the 33rd Annual ACM Conference Extended Abstracts on Human Factors in Computing Systems - CHI EA ’15 (pp. 2241–2246). https://doi.org/10.1145/2702613.2732754
\end{reference}





## Personality as user similarity

- new user problem
- N = 52
- images = 70
- neighborhood-based RS: Euclidian distance $^{-1}$

![](./figures/personality-p.png){width=60%}

\begin{reference}{}
Tkalčič, M., Kunaver, M., Košir, A., and Tasič, J. (2011). Addressing the new user problem with a personality based user similarity measure. In F. Ricci, G. Semeraro, M. de Gemmis, P. Lops, J. Masthoff, F. Grasso, J. Ham (Eds.), Joint Proceedings of the Workshop on Decision Making and Recommendation Acceptance Issues in Recommender Systems (DEMRA 2011) and the 2nd Workshop on User Models for Motivational Systems: The affective and the rational routes to persuasion (UMMS 2011).
\end{reference}




## Personality as user similarity

- N = 113
- 646 songs
- TIPI
- user similarities (Pearson CC)
	- item-based
	- personality-based


![](./figures/ronghu.png){width=60%}

\begin{reference}{}
Hu, R., and Pu, P. (2010). Using Personality Information in Collaborative Filtering for New Users. In Proceedings of the 2nd ACM RecSys’10 Workshop on Recommender Systems and the Social Web (pp. 17–24).
\end{reference}




## Personality in Matrix Factorization

- in (Elahi et al., 2013) and (Fernández-Tobías, 2016)
- injection of personality factors in MF as additional (latent) features (a la SVD++)

[columns]

[column=0.5]

$$r_{ui}  = q_i ( p_u + \sum_{a \in A(u)} y_a )$$

[column=0.5]

![](./figures/fernandez.png)

[/columns]

- personality $u = (2.3, 4.0, 3.6, 5.0, 1.2)$ maps to A(u) = {ope2, con4, ext4, agr5, neu1}.
- (Fernández-Tobías, 2016) is a very comprehensive paper 
- iMF = (Hu et al., 2008)


\begin{reference}{}
Elahi, M., Braunhofer, M., Ricci, F., and Tkalčič, M. (2013). Personality-based active learning for collaborative filtering recommender systems. In M. Baldoni, C. Baroglio, G. Boella, and O. Micalizio (Eds.), AI*IA 2013: Advances in Artificial Intelligence (pp. 360–371). 

Fernández-Tobías, I., Braunhofer, M., Elahi, M., Ricci, F., and Cantador, I. (2016). Alleviating the new user problem in collaborative filtering by exploiting personality information. User Modeling and User-Adapted Interaction, 26(2), 1–35. https://doi.org/10.1007/s11257-016-9172-z
\end{reference}





## Personality and Diversity

- within subject N=52
- movies
- diversity per: genre, director, country, release time, actor
- rules from a previous study
	- High Level of Openness is linked to high need for diversity w.r.t. ―actor/actress
	- Low Level of Conscientiousness is correlated with high need for the overall diversity



![](./figures/wu-chen.jpg)

\begin{reference}{}
Wu, W., Chen, L., and He, L. (2013). Using personality to adjust diversity in recommender systems. Proceedings of the 24th ACM Conference on Hypertext and Social Media - HT ’13, (May), 225–229. 

Chen, L., Wu, W., and He, L. (2013). How personality influences users’ needs for recommendation diversity? CHI ’13 Extended Abstracts on Human Factors in Computing Systems on - CHI EA ’13, 829. https://doi.org/10.1145/2468356.2468505
\end{reference}





## Group RecSys 

[columns]

[column=0.5]

![](./figures/amra-1.png){width=70%}

[column=0.5]

![](./figures/amra-4.png){width=70%}

[/columns]

. . .


- cmw - conflict mode weight (users assumed to not be prepared to downgrade their ratings receive a corresponding positive adaptation.)

$$cmw(u) = {1 + assertiveness(u) + cooperativeness(u) \over 2}$$
$$p_{pers}(u_a,i) = {\sum_{u \in G(u \neq u_a)} ( p(u_a,i) + (cmw(u_a) - cmw(u))) \over |G| -1}$$

## Group RecSys

![](./figures/amra-3.png){width=100%}

\begin{reference}{}
Tkalcic, M., Delic, A., \& Felfernig, A. (2018). Personality, Emotions, and Group Dynamics. In A. Felfernig, L. Boratto, Martin Stettinger, \& M. Tkalcic (Eds.), Group Recommender Systems An Introduction (pp. 157-167). https://doi.org/10.1007/978-3-319-75067-5\_9

L. Quijano-Sanchez, D. Bridge, B. Diaz-Agudo, J. Recio-Garcia, A case-based solution to the cold-start problem in group recommenders, in 23rd International Conference on Artificial Intelligence (IJCAI 2013) (2013), pp. 3042-3046
\end{reference}







## Other resources

- personality and cross domain (Cantador et al., 2013)
- Amazon MF + personality from Twitter (Adamopoulos and Todri, 2015)
- UMUAI, June 2016, Special Issue on Personality in Personalized Systems, editors: Marko Tkalcic, Daniele Quercia, Sabine Graf 
- chapter in the RecSys Handbook



\begin{reference}{}
Cantador, I., Fernández-tobías, I., and Bellogín, A. (2013). Relating Personality Types with User Preferences in Multiple Entertainment Domains. EMPIRE 1st Workshop on “Emotions and Personality in Personalized Services”, 10. June 2013, Rome.

Adamopoulos, Panagiotis, and Vilma Todri. "Personality-Based Recommendations: Evidence from Amazon. com." RecSys Posters. 2015.

Tkalčič, M., Quercia, D., and Graf, S. (2016). Preface to the special issue on personality in personalized systems. User Modeling and User-Adapted Interaction, 26(2–3), 103–107. https://doi.org/10.1007/s11257-016-9175-9

Tkalcic, M., \& Chen, L. (2015). Personality and Recommender Systems. In F. Ricci, L. Rokach, \& B. Shapira (Eds.), Recommender Systems Handbook (2nd ed., Vol. 54, pp. 715–739). Boston, MA: Springer US. https://doi.org/10.1007/978-1-4899-7637-6\_21
\end{reference}





























## Acquisition of Personality

- questionnaires
- datasets
- off-the shelf tools



## Questionnaires

- Extensive questionnaires (from 5 to several 100s questions)
	- BFI: 44 questions
	- TIPI : 10 questions
	- NEO-IPIP: 300 questions
- For each user $u$ a five tuple $b =(b_1, b_2, b_3, b_4, b_5)$




## TIPI - Ten-Items Personality Inventory

[columns]

[column=0.4]

TIPI:
I see myself as (1-7 ... agree/disagree):

1. Extraverted, enthusiastic. 
2. Critical, quarrelsome. 
3. Dependable, self-disciplined. 
4. Anxious, easily upset. 
5. Open to new experiences, complex. 
6. Reserved, quiet.
7. Sympathetic, warm. 
8. Disorganized, careless. 
9. Calm, emotionally stable. 
10. Conventional, uncreative.  

[column=0.6]

- $b_1 = q_1 + (8-q_6)$ = Extraversion
- $b_2 = q_2 + (8-q_7)$ = Agreeableness
- $b_3 = q_3 + (8-q_8)$ = Conscientiousness
- $b_4 = q_4 + (8-q_9)$ = Emotional Stability
- $b_5 = q_5 + (8-q_{10})$ = Openness to Experiences

[/columns]


\begin{reference}{}
Gosling, S. D., Rentfrow, P. J., and Swann, W. B. (2003). A very brief measure of the Big-Five personality domains. Journal of Research in Personality, 37(6), 504–528. doi:10.1016/S0092-6566(03)00046-1
\end{reference}




## Let's measure ourselves

![](./figures/TIPI_RecSys.png){width=100%}



## Datasets 

- movielens: 1840 users, 985k ratings, 38k movies
- myPersonality (not available anymore)
- Data Mining Tutorial (Kosinski et al.): 110k users, 1.5M likes, 10M interactions

![](./figures/likes.png){width=50%}

![](./figures/users.png){width=50%}

![](./figures/users_likes.png){width=45%}

\begin{reference}{}
https://grouplens.org/blog/different-strokes-for-different-folks-the-value-of-personality-type-in-recommender-systems-and-social-computing/

http://www.michalkosinski.com/data-mining-tutorial

Odić, A., Košir, A., \& Tkalčič, M. (2016). Affective and Personality Corpora. In M. Tkalcic, B. De Carolis, M. de Gemmis, A. Odic, \& A. Košir (Eds.), Emotions and Personality in Personalized Services (pp. 163–178). Springer. https://doi.org/10.1007/978-3-319-31413-6\_9
\end{reference}   


## Off-the-shelf Tools

- https://applymagicsauce.com/demo
- IBM Watson Personality Insights
	- https://personality-insights-demo.ng.bluemix.net/
	- @Oprah

```json
{
  "word_count": 15128,
  "processed_language": "en",
  "personality": [
    {
      "trait_id": "big5_openness",
      "name": "Openness",
      "category": "personality",
      "percentile": 0.8048087217136444,
      "significant": true,
    }
    ...
    {
      "trait_id": "big5_conscientiousness",
      "name": "Conscientiousness",
      "category": "personality",
      "percentile": 0.8102947333861581,
      "significant": true,
    }
```

























# Emotions-based Recommendations

## Emotions-based Recommendations

- assume emotions can be measured

. . .

![](./figures/framework.png){width=70%}


\begin{reference}{}
Tkalčič, M., Košir, A., Tasič, J., and Kunaver, M. (2011). Affective recommender systems: the role of emotions in recommender systems. In A. Felfernig, L. Chen, M. Mandl, M. Willemsen, D. Bollen, and M. Ekstrand (Eds.), Joint proceedings of the RecSys 2011 Workshop on Human Decision Making in Recommender Systems (Decisions@RecSys’11) and User-Centric Evaluation of Recommender Systems and Their Interfaces-2 (UCERSTI 2) affiliated with the 5th ACM Conference on Recommender (pp. 9–13).
\end{reference}



## Emotions as context

- task: recommending music content that fits a place of interest (POI)
- emotional tags (GEMS) attached by a users’ population to both music and POIs
- tag-based similarity metrics to match music with POI


\begin{reference}{}
Marius Kaminskas and Francesco Ricci. 2011. Location-adapted music recommendation using tags. Proceedings of UMAP '11, Springer-Verlag, Berlin, Heidelberg, 183-194.

Kaminskas, M., and Ricci, F. (2016). Emotion-Based Matching of Music to Places (pp. 287–310). In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odić, and A. Košir (Eds.), Emotions and Personality in Personalized Services: Models, Evaluation and Applications
\end{reference}



## Emotions as context

- CoMoDa dataset
- various contextualization techniques

```ini
Context variables:
time : Morning, Afternoon, Evening, Night
daytype : Working day, Weekend, Holiday
season : Spring, Summer, Autumn, Winter
location :	Home, Public place, Friend's house
weather : Sunny / clear, Rainy, Stormy, Snowy, Cloudy
social : Alone, My partner, Friends, Colleagues, Parents, Public, My family
endEmo : Sad, Happy, Scared, Surprised, Angry, Disgusted, Neutral
dominantEmo : Sad, Happy, Scared, Surprised, Angry, Disgusted, Neutral	
mood : Positive, Neutral, Negative
physical : Healthy, Ill	
decision : User decided which movie to watch, User was given a movie
interaction : first interaction with a movie, n-th interaction with a movie
```

\begin{reference}{}
Yong Zheng, Bamshad Mobasher, Robin D. Burke: The Role of Emotions in Context-aware Recommendation. Decisions@RecSys 2013: 21-28

Zheng, Y., Mobasher, B., and Burke, R. (2016). Emotions in Context-Aware Recommender Systems (pp. 311–326). In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odić, and A. Košir (Eds.), Emotions and Personality in Personalized Services: Models, Evaluation and Applications
\end{reference}

## Emotions as context

![](./figures/zheng.png){width=70%}

\begin{reference}{}
Yong Zheng, Bamshad Mobasher, Robin D. Burke: The Role of Emotions in Context-aware Recommendation. Decisions@RecSys 2013: 21-28

Zheng, Y., Mobasher, B., and Burke, R. (2016). Emotions in Context-Aware Recommender Systems (pp. 311–326). In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odić, and A. Košir (Eds.), Emotions and Personality in Personalized Services: Models, Evaluation and Applications
\end{reference}




## Affective User Modeling

- Multimedia content ELICITS (induces) emotions
- Underlying assumption: users differ in their preferences for emotions

[columns]

[column=0.5]

![](./figures/calm.png){width=90%}

[column=0.5]

![](./figures/anxiety.png){width=70%}

[/columns]

## Affective User Modeling

![](./figures/affusermodel.png){width=70%}


\begin{reference}{}
Tkalčič, M., Burnik, U., and Košir, A. (2010). Using affective parameters in a content-based recommender system for images. User Modeling and User-Adapted Interaction, 20(4), 279–311. doi:10.1007/s11257-010-9079-z

Tkalčič, M., Odić, A., Košir, A., and Tasič, J. (2013). Affective labeling in a content-based recommender system for images. IEEE Transactions on Multimedia, 15(2), 391–400. https://doi.org/10.1109/TMM.2012.2229970
\end{reference}





## Emotions as feedback 

- video-on-demand scenario
- usage of **hesitation** as feedback
- 4 recommendations, 1 selection
	- control group: recommend similar
	- hesitation group: recommend similar/diverse
- quality of experience (QoE) is improved when hesitation is taken into account


![](./figures/vodlan.png){width=70%}


\begin{reference}{}
Vodlan, T., Tkalčič, M., and Košir, A. (2015). The impact of hesitation, a social signal, on a user’s quality of experience in multimedia content retrieval. Multimedia Tools and Applications. doi:10.1007/s11042-014-1933-2
\end{reference}




## Emotional contagion

- RQ: does emotional contagion occur outside of in-person interactions?
- Facebook users (N = 689,003)
- 2 experiments:
	- exposure to friends’ positive emotional content was reduced
		- group (only emotional content omitted)
		- control group (any content omitted)
	- exposure to friends’ negative emotional content was reduced
		- group (only emotional content omitted)
		- control group (any content omitted)

![](./figures/fb-contagion.png)











## Acquisition of Emotions

- questionnaires
- datasets
- multimodal detection




## Datasets

- Small scale
	- LDOS PerAff-1 
	- LDOS CoMoDa



## Multimodal Emotion Detection

[columns]

[column=0.5]

- modalities
	- audio
	- language
	- visual - videos of faces (action units)
	- physiology
	- brain signals

[column=0.5]

- target emotion
	- discrete (classification)
	- continuous (regression)

[/columns]

![](./figures/schuller.png){width=70%}

\begin{reference}{}
Schuller, B. W. (2016). Acquisition of Affect. In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odić, and A. Košir (Eds.), Emotions and Personality in Personalized Services: Models, Evaluation and Applications (pp. 57–80). Cham: Springer International Publishing. 
\end{reference}



## Problems with emotion detection

- illumination
- face obfuscation

. . .

[columns]

[column=0.45]

![](./figures/ekman_emotions.png)

[column=0.55]

. . .

![](./figures/plain_emotions.png){width=95%}

[/columns]



## Academic semi-solutions for emotion recognition

[columns]

[column=0.4]

- many research groups offer their code
- usual prototype-related limitations
	- robustness
	- support


[column=0.6]

![](./figures/ibug_pipeline.png)

[/columns]

New APIs are coming out all the time. A recent list http://nordicapis.com/20-emotion-recognition-apis-that-will-leave-you-impressed-and-concerned/

- https://ibug.doc.ic.ac.uk/resources/action-unit-detector-2016/
- http://affect.media.mit.edu/software.php
- many, many more



## Measuring Emotions - off-the-shelf solutions

- Noldus FaceReader
- Affectiva
- Amazon Rekognition
- IBM Tone Analyzer


























# Hands-on: Data Acquisition

## Goal

- Problem statement: lack of recsys datasets with emotional feedback
- Solution: To set-up a web app for acquiring emotions during RecSys sessions, hence enabling large-scale acquisition of recsys interactions.

. . .

- How? Using a public API for emotion detection. 



## Pre-requisites

In order to be able to implement the solution you should have familiarity with programming. The tutorial will be on web client-side (HTML/DOM/Javascript), although you may use another solution (Android, iOS/macOS, Linux, Unity, Windows).


Before the session starts please make sure that you:

- have a laptop with an integrated camera
- have the Chrome browser installed
- (optionally) installed an HTTP server on your laptop
	- apache
	- MAMP/XAMPP or some other integrated solution
	- know where your ```htdocs``` folder is




## Scenario

![](./figures/handson.png)

- Affectiva SDK : https://developer.affectiva.com/
- we will build an HTML/Javascript page
- the full code is available at https://gitlab.com/markotka/recsys2018-tutorial

## Step 1

- run the HTTP server
- create a subdirectory in the ```htdocs``` directory (e.g. ```.../htdocs/RecSys_demo```)

## Step 2

- in a text editor create the blank HTML page (e.g. ```.../htdocs/RecSys_demo/demo-1.html```)

```html
<!DOCTYPE html>
<html>
<head>
	<title>ACM RecSys Tutorial 2018 Demo</title>
</head>
<body>

</body>
</html>
``` 

## Step 3

- in your web browser open the ```demo-1.html``` page

```ini
http://localhost/RecSys_demo/demo-1.html

```



## Step 4

- create three buttons
- create the three visualization boxes 

```html
<!DOCTYPE html>
<html>
<head>
</head>
<body>
	<button id="start" onclick="onStart()">Start</button>
	<button id="stop" onclick="onStop()">Stop</button>
	<button id="reset" onclick="onReset()">Reset</button>
	<hr>

	<div id="content" style="border: 1px solid; width: 400px; height: 620px; float: left; margin: 10px; padding: 0;">

		CONTENT

	</div>
	
	<div id="preview" style="border: 1px solid; width: 400px; height: 300px; float: left; margin: 10px; padding: 0;"> 
		
	</div>

	<div id="results" style="border: 1px solid; width: 400px; height: 700px; float: left; margin: 10px; padding: 0;">
		
	</div>

</body>
</html>
```

## Step 5

- Check your HTML code in the web browser
	- after each modification to the HTML file go back to the browser, refresh the page and observe the effects of the change




## Step 6

- Add the needed SDKs to the head of the HTML

```html
<head>
	<title>ACM RecSys Tutorial 2018 Demo</title>
  	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  	<script type="text/javascript" src="//code.jquery.com/jquery-3.1.0.js"/></script>
  	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"/></script>
    <script type="text/javascript" src="https://download.affectiva.com/js/3.1/affdex.js"/></script>
</head>

```

For reference, see https://knowledge.affectiva.com/docs/getting-started-with-the-emotion-sdk-for-javascript



## Step 7

- start a ```<script>``` element in the bottom of the HTML

```html
...
	<div id="results" style="border: 1px solid; width: 400px; height: 700px; float: left; margin: 10px; padding: 0;">
		
	</div>

<script type="text/javascript"></script>
...
```



## Step 8

- set up the **detector**


```javascript
...
// Link a div for previewing the camera feed
var divRoot = $("#preview")[0];

// Configuration
var width = 400;
var height = 300;
var faceMode = affdex.FaceDetectorMode.LARGE_FACES;

//Construct a CameraDetector 
var detector = new affdex.CameraDetector(divRoot, width, height, faceMode);

...
```



## Step 9

- Choose the classifiers
	- The next step is to turn on the detection of the metrics needed. 
	- list of classifiers: http://developer.affectiva.com/metrics/

```javascript
...
detector.detectAllExpressions();
detector.detectAllEmotions();
detector.detectAllEmojis();
detector.detectAllAppearance();
...
```



## Step 10

- add the callback functions for the three buttons

```javascript
...
//function executes when Start button is pushed.
function onStart() {
	if (detector && !detector.isRunning) {
		detector.start();
	}
}

//function executes when the Stop button is pushed.
function onStop() {
	if (detector && detector.isRunning) {
		detector.removeEventListener();
		detector.stop();
	}
};

//function executes when the Reset button is pushed.
function onReset() {
	if (detector && detector.isRunning) {
		detector.reset();
		$('#results').html("");
	}
};
...
```



## Step 11

- Configure the **callback functions**
	- The Detectors use callbacks to communicate events and results. 
	- For each action there are two callbacks. 
		- A success callback is called when an action successfully completes, and 
		- a failure callback is called in case of an action failure.
	- The functions addEventListener and removeEventListener are used to register or deregister a callback.

```javascript
...
//Add a callback to notify when the detector is initialized and ready for runing.
detector.addEventListener("onInitializeSuccess", function() {
	//The following two objects are oart of the Affectiva SDK
	//Display canvas instead of video feed because we want to draw the feature points on it
	$("#face_video_canvas").css("display", "block");
	$("#face_video").css("display", "none");
});
...
```




## Step 12

```javascript
...
//Add a callback to receive the results from processing an image.
//The faces object contains the list of the faces detected in an image.
//Faces object contains probabilities for all the different expressions, emotions and appearance metrics
detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {
	if (faces.length > 0) {
		var resultsHtml = "";
		resultsHtml += "Number of faces found: " + faces.length;
		resultsHtml += "<br>Appearances: " + JSON.stringify(faces[0].appearance);
		resultsHtml += "<br>Emotions: " + JSON.stringify(faces[0].emotions);
		resultsHtml += "<br>Expressions: " + JSON.stringify(faces[0].expressions);
		resultsHtml += "<br>Emoji: " + faces[0].emojis.dominantEmoji;
		var htmlToDisplay = resultsHtml.replace(/,/g,",<br>");	// make output nicer
		$('#results').html(htmlToDisplay);
		drawFeaturePoints(image, faces[0].featurePoints);
	}

});

...
```


## Step 13


- add the drawFeaturePoints method

```javascript
...
//Draw the detected facial feature points on the image
function drawFeaturePoints(img, featurePoints) {
	var contxt = $('#face_video_canvas')[0].getContext('2d');

	var hRatio = contxt.canvas.width / img.width;
	var vRatio = contxt.canvas.height / img.height;
	var ratio = Math.min(hRatio, vRatio);

	contxt.strokeStyle = "#FFFFFF";
	for (var id in featurePoints) {
		contxt.beginPath();
		contxt.arc(featurePoints[id].x,
		featurePoints[id].y, 2, 0, 2 * Math.PI);
		contxt.stroke();

	}
}
...
```



## Check the result in the browser


![](./figures/demo-gui.png)



## Context-aware RecSys - 14

- demo-2.html
- modify the function ```detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {``` 

```javascript
detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {
	if (faces.length > 0) {
		var resultsHtml = "";
		resultsHtml += "Number of faces found: " + faces.length;
		resultsHtml += "<br>Appearances: " + JSON.stringify(faces[0].appearance);
		resultsHtml += "<br>Emotions: " + JSON.stringify(faces[0].emotions);
		resultsHtml += "<br>Expressions: " + JSON.stringify(faces[0].expressions);
		resultsHtml += "<br>Emoji: " + faces[0].emojis.dominantEmoji;
		var htmlToDisplay = resultsHtml.replace(/,/g,",<br>");	// make output nicer
		$('#results').html(htmlToDisplay);
		drawFeaturePoints(image, faces[0].featurePoints);

		// show recommendations every N frames
		N = 50;
		rec_cnt++;
		if (rec_cnt > N){
			rec_cnt = 0;
			recommendByContext(faces);
		}
	}
});

```

## Context-aware RecSys - step 15

- add a primitive context-aware RecSys

```javascript
function recommendByContext(emotionsJson){
	var html = "Recommendations<br>";
	var joy = emotionsJson[0].emotions.joy;
	var sadness = emotionsJson[0].emotions.sadness;
	if (joy > 20){
		html += '<iframe src="https://www.youtube.com/embed/iKCw-kqo3cs" allowfullscreen="" width="245"
		height="173.33333333333334" frameborder="0"></iframe>';
	}
	else if (sadness > 20){
		html += '<iframe src="https://www.youtube.com/embed/NxQmuJnrjxg" allowfullscreen="" width="245" 
		height="173.33333333333334" frameborder="0"></iframe>';
	}
	else {
		html += '<iframe src="https://www.youtube.com/embed/rryMP7aJ12o" allowfullscreen="" width="245" 
		height="173.33333333333334" frameborder="0"></iframe>';
	}
	$('#content').html(html);
}
```
\begin{reference}{}
Yong Zheng, Bamshad Mobasher, Robin D. Burke: The Role of Emotions in Context-aware Recommendation. Decisions@RecSys 2013: 21-28

Zheng, Y., Mobasher, B., and Burke, R. (2016). Emotions in Context-Aware Recommender Systems (pp. 311–326). In M. Tkalčič, B. De Carolis, M. de Gemmis, A. Odić, and A. Košir (Eds.), Emotions and Personality in Personalized Services: Models, Evaluation and Applications
\end{reference}



## Check the result in the browser


![](./figures/demo-gui-2.png)



# Wrap-up


## Conclusion

- emotions and personality account for differences in user behavior
- research is still scattered
- useful in various aspects of the recsys interaction

## Open Issues

- lack of awareness
	- this tutorial will hopefully help

- lack of data
	- mostly small-scale data gathered through user studies

- unobtrusive annotation of content
	- subtitles?

- privacy

## Thank you

- thank you for attending
- open for collaborations/internships
- Q&A
 






























